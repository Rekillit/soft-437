import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

/**
 * Instrumentation
 */
public class Instrumentation {
    private Deque<Long> timeStack;
    private long startTime;
    private boolean state;
    private StringBuilder msg;
    private ArrayList<Long> times;

    private static Instrumentation instance;

    // MUST FOLLOW SINGLETON PATTERN
    private Instrumentation() {
        this.timeStack = new ArrayDeque<Long>();
        this.startTime = System.nanoTime();
        this.times = new ArrayList<Long>(5);
        this.state = false;
        this.msg = new StringBuilder();
    }

    public static Instrumentation Instance() {
        if (instance == null) {
            instance = new Instrumentation();
        }
        return instance;
    }

    public void activate(boolean state) {
        this.state = state;
    }

    public void startTiming(String comment) {
        if (this.state) {
            this.initLine();
            this.msg.append("STARTTIMING: " + comment + "\n");
            this.timeStack.add(System.nanoTime());
        }
    }

    public void stopTiming(String comment) {
        if (this.state) {
            times.add((System.nanoTime() - this.timeStack.removeLast()) / 1000000);
            String buf = ("STOPTIMING: " + comment + " " + times.get(times.size() - 1) + "ms");
            this.initLine();
            this.msg.append(buf + "\n");
        }
    }

    public void comment(String comment) {
        if (this.state) {
            this.initLine();
            this.msg.append("COMMENT: " + comment);
        }
    }

    private void initLine() {
        if (this.state) {
            for (int i = 0; i < timeStack.size(); i++) {
                this.msg.append("|" + "\t");
            }
        }
    }

    public void dump(String filename) throws IOException {
        this.msg.append("TOTAL TIME: " + (System.nanoTime() - this.startTime) / 1000000 + "ms" + "\n");
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        writer.write(msg.toString());
        writer.close();
    }

    public void dump() throws IOException {
        this.dump("output.log");

    }

    public String popTimes() {
        if (this.state) {
            String str = this.times.toString().substring(1, times.toString().length() - 1);
            this.times = new ArrayList<Long>(5);
            return str;
        }
        return "";
    }
}