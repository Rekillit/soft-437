import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import algs.BubbleSort2Algorithm;
import algs.ExtraStorageMergeSortAlgorithm;
import algs.NaiveQuickSortAlgorithm;
import algs.SelectionSortAlgorithm;
import algs.SortAlgorithm;

public class Test {
    private ArrayList<String> data;
    private int arrSize;
    private boolean logcsv;
    private SortAlgorithm alg;

    public static enum sortEnum {
        SELECTION, BUBBLE, QUICK, MERGE, DUMMY_NO_OVERHEAD, DUMMY_OVERHEAD, POPULATE, MAIN,
        SELECTION_NO_OVERHEAD, BUBBLE_NO_OVERHEAD, QUICK_NO_OVERHEAD, MERGE_NO_OVERHEAD
    };

    // CHANGE THESE CONSTANTS FOR SIMULATION
    private static final int ARRAY_SIZE = 64000;
    private static final int NUM_ITERATIONS = 10;
    private static final sortEnum CURR_SORT = sortEnum.QUICK_NO_OVERHEAD;

    public Test(int arrSize) {
        this.arrSize = arrSize;
        this.logcsv = false;

        switch (Test.CURR_SORT) {
            case SELECTION:
            case SELECTION_NO_OVERHEAD:
                alg = new SelectionSortAlgorithm();
                break;

            case BUBBLE:
            case BUBBLE_NO_OVERHEAD:
                alg = new BubbleSort2Algorithm();
                break;

            case QUICK:
            case QUICK_NO_OVERHEAD:
                alg = new NaiveQuickSortAlgorithm();
                break;

            case MERGE:
            case MERGE_NO_OVERHEAD:
                alg = new ExtraStorageMergeSortAlgorithm();
                break;
        }
    }

    public Test(int arrSize, boolean logcsv) {
        this.arrSize = arrSize;
        this.logcsv = logcsv;

        if(logcsv){
            this.data = new ArrayList<String>();
        }
    }

    public int[] populateArray() {

        int[] arr = new int[this.arrSize];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = ThreadLocalRandom.current().nextInt(1, 99999);
        }
        return arr;
    }

    public void setArrSize(int size) {
        this.arrSize = size;
    }

    public void addData(String str) {
        if(this.logcsv){
            this.data.add(str);
        }
    }

    public void prependData(String str) {
        if(this.logcsv){
            this.data.set(data.size() - 1, str + ", " + data.get(data.size() - 1));
        }
    }

    public void dumpCSV(String filename) throws IOException {
        if(logcsv){
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
            for (String line : this.data) {
                writer.append(line + "\n");
            }
            writer.close();
        }
    }

    public void dumpCSV() throws IOException {
        this.dumpCSV("out.csv");
    }

    public static void main(String[] args) {
        Instrumentation ins = Instrumentation.Instance();

        Test test = new Test(Test.ARRAY_SIZE);

        ins.activate(true);
        ins.startTiming("Main");

        // for (int k = 1000; k < 270000; k = k * 2) { // ArraySize
        // test.setArrSize(k);

        ins.startTiming("Loop");
        for (int j = 0; j < Test.NUM_ITERATIONS; j++) { // Iterations

            switch (Test.CURR_SORT) {
            case SELECTION:
                ins.startTiming("Selection");
                test.alg.sort(test.populateArray());
                ins.stopTiming("Selection");

                // this.addData(Integer.toString(selection.getTotalMoves()));
                break;

            case BUBBLE:
                ins.startTiming("Bubble");
                test.alg.sort(test.populateArray());
                ins.stopTiming("Bubble");

                // this.addData(Integer.toString(selection.getTotalMoves()));
                break;

            case QUICK:
                ins.startTiming("Quick");
                test.alg.sort(test.populateArray());
                ins.stopTiming("Quick");

                // this.addData(Integer.toString(selection.getTotalMoves()));
                break;

            case MERGE:
                ins.startTiming("Quick");
                test.alg.sort(test.populateArray());
                ins.stopTiming("Quick");

                // this.addData(Integer.toString(selection.getTotalMoves()));
                break;
            
            case DUMMY_NO_OVERHEAD:
                System.out.println("Dummy constant time operation");
                break;

            case DUMMY_OVERHEAD:
                ins.startTiming("Dummy");
                System.out.println("Dummy constant time operation");
                ins.stopTiming("Dummy");
                break;
            
            case POPULATE:
                ins.startTiming("PopulateArray");
                test.populateArray();
                ins.stopTiming("PopulateArray");
                break;
            
            case SELECTION_NO_OVERHEAD:
            case BUBBLE_NO_OVERHEAD:
            case QUICK_NO_OVERHEAD:
            case MERGE_NO_OVERHEAD:
                test.alg.sort(test.populateArray());
                break;
            
            case MAIN:
                break;
            }

            // test.prependData(test.arrSize + ", " + ins.popTimes());
        }
        ins.stopTiming("Loop");
        // }

        ins.stopTiming("Main");

        try {
            // test.dumpCSV("output.csv");
            ins.dump("output.log");
        } catch (Exception e) {
            System.out.println("Error Logging");
        }
    }
}